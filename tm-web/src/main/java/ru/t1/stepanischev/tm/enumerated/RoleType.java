package ru.t1.stepanischev.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}