package ru.t1.stepanischev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanischev.tm.api.repository.IProjectWebRepository;
import ru.t1.stepanischev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanischev.tm.exception.user.AccessDeniedException;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectWebRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final ProjectDTO model, @Nullable final String userId) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Nullable
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    public ProjectDTO findOneById(@Nullable String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    public ProjectDTO findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void remove(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

    @Transactional
    public void removeByUserId(@Nullable final ProjectDTO model, @Nullable final String userId) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void update(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

}