package ru.t1.stepanischev.tm.exception.user;

public class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Authentication Error...");
    }

}