package ru.t1.stepanischev.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(final String cmdName) {
        super("Error! Command '" + cmdName + "' is not supported...");
    }

}