package ru.t1.stepanischev.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException{

    public ArgumentNotSupportedException() {
    }

    public ArgumentNotSupportedException(final String argName) {
        super("Error! Argument '" + argName + "' is not supported...");
    }

}