package ru.t1.stepanischev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
public class CustomUser extends User {

    @Nullable
    private String userId;

    @NotNull
    public CustomUser withUserId(@Nullable final String userId) {
        this.userId = userId;
        return this;
    }

    public CustomUser(@NotNull final UserDetails user) {
        super(user.getUsername(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(),
                user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
    }

    public CustomUser(
            @Nullable final String username,
            @Nullable final String password,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, authorities);
    }

    public CustomUser(
            @Nullable final String username,
            @Nullable final String password,
            final boolean enabled, final boolean accountNonExpired,
            final boolean credentialsNonExpired,
            final boolean accountNonLocked,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

}