package ru.t1.stepanischev.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id not found...");
    }

}