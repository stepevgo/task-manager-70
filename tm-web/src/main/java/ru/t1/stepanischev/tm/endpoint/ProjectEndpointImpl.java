package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.stepanischev.tm.model.ProjectDTO;
import ru.t1.stepanischev.tm.service.ProjectService;
import ru.t1.stepanischev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.stepanischev.tm.api.endpoint.IProjectRestEndpoint")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    @WebMethod
    public @Nullable Collection<ProjectDTO> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/findById/{id}")
    @WebMethod
    public @Nullable ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/delete")
    @WebMethod
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final ProjectDTO project
    ) {
        projectService.removeByUserId(project, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @WebMethod
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    @WebMethod
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final ProjectDTO project
    ) {
        projectService.addByUserId(project, UserUtil.getUserId());
        return project;
    }

}
