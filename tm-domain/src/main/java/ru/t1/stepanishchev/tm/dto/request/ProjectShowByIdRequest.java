package ru.t1.stepanishchev.tm.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdRequest extends AbstractIdRequest {

    public ProjectShowByIdRequest(
            @Nullable final String token,
            @Nullable final String id
    ) {
        super(token, id);
    }

}