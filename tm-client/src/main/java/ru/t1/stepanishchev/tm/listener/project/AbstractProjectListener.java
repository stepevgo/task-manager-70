package ru.t1.stepanishchev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.IProjectEndpoint;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    public IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
