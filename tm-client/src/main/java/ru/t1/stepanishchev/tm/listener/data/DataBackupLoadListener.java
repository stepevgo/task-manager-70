package ru.t1.stepanishchev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.dto.request.DataBackupLoadRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public final static String NAME = "backup-load";

    @NotNull
    private final static String DESCRIPTION = "Load backup from file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Load backup");
        domainEndpoint.loadDataBackup(new DataBackupLoadRequest(getToken()));
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}