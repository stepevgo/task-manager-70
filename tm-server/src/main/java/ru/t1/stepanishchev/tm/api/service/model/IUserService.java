package ru.t1.stepanishchev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.model.User;

import java.util.Collection;
import java.util.List;

@Service
public interface IUserService extends IAbstractService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findOneByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

    void set(@NotNull Collection<User> users);

    void clear();

}