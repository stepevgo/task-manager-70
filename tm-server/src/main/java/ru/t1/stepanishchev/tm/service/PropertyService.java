package ru.t1.stepanishchev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private String serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private  String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.sql_dialect']}")
    private String dBDialect;

    @NotNull
    @Value("#{environment['database.url']}")
    private String dBUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String dBUser;

    @NotNull
    @Value("#{environment['database.password']}")
    private String dBPassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dBHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String dBShowSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    private String dBSecondLvlCache;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String dBFactoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    private String dBUseQueryCache;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String dBUseMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String dBRegionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String dBHazelConfig;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String dBFormatSql;

    @NotNull
    @Value("#{environment['database.comment_sql']}")
    private String dBCommentsSql;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

}
