package ru.t1.stepanishchev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.Collection;
import java.util.List;

@Service
public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    String getSortType(@Nullable ProjectSort sort);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Project removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void set(@NotNull Collection<Project> projects);

    @NotNull
    Project updateOneById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}